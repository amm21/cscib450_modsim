% Assignment 3
% Fall 2019
% Modeling & Simulation
% Matthew Holmes, Alexis Miller, Jarod Valvo, & Katie Redmond
clear;
clc;

% names of sound files
names = ['Holmes.wav', 'Redmond.wav', 'Valvo.wav', 'Miller.wav', 'Painter.wav', 'Cheatham.wav', 'Cheatham_2.wav'];

% set the window variables
window = 1024;  
noverlap=window/2;
nfft=window*2;


nameStart = 1;
%finds the index of the "." for all .wav
nameEnd = strfind(names,'.wav');

% the for loop will run for every .wav string in names array
for i = 1:length(strfind(names, '.wav'))
    %get the name.wav from the names array
    currentFile = names(nameStart:nameEnd(i)+3);
    % the name to be displayed on the graph
    dispName = names(nameStart:nameEnd(i)-1);
    
    %update start index for the next name
    nameStart = nameEnd(i)+4;
    % read in the given file for audio analysis
    [x,fs]=audioread(currentFile);
    [s,f,t,ps] = spectrogram(x,window,noverlap,nfft,fs,'yAxis');
    % subplot is the plot for each .wav file
    subplot(4,2,i);
    surf(t,f,10*log10(ps), 'edgecolor', 'none'); axis tight; view(-10, 30);
    colormap(hot);
    set(gca,'clim', [-80 -30]);
    % display labels for the drawn graph
    xlabel('Seconds'); ylabel('Frequencies');title(dispName);
end