%{
Alexis Miller
CSCI B450 Mod & Sim
Project 5: Inventory 
%}

clear;
clc;

s = 20;
S = 80;
workInv = 80;

% get the data and set up
file = fopen('sis1.dat');
custOrders = fscanf(file, '%d');
day = 1:100;
invLevel = [];
toOrder = [];

% for optimal s later
setupCost = 1000;
holdingCost = 25; % per day
shortageCost = 700; % per day


for i = 1:100
    
    % find your working inventory
    workInv = workInv - custOrders(i);
    
    % if you fell below minimum
    if workInv < s
        
        % get the required order amount 
        orderReq = S - workInv;
        
        % if you need to order S or more, max is S
        if orderReq > S
            orderReq = S;
        end
        
        % append values and re-calc working inventory so you
        % don't append a negative value (refills are instant from above)
        toOrder = [toOrder, orderReq];
        workInv = S - custOrders(i); 
        invLevel = [invLevel, workInv];
        
    else
        
        % append values, no extra stuff needed since you didn't
        % fall below the minimum
        toOrder = [toOrder, 0];
        invLevel = [invLevel, workInv];
        
    end
    
end

% make a scatter plot
scatter(day, invLevel, 20, 'b', 'filled');
xlabel('Day')
ylabel('InvLevel')

% re-orient for table use
day = day';
invLevel = invLevel';
toOrder = toOrder';

% make them into a table
invTable = table(day, custOrders, invLevel, toOrder);

% close file when done
fclose(file);

% write the table to excel
writetable(invTable, 'amm21_csci450_project5.xlsx');