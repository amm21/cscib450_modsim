%{
Alexis Miller
CSCI B450 Mod & Sim 
Project 5: Inventory (550 Extra Credit)
%}

clear;
clc;

cost = 0;
S = 80;
workInv = 80;

% get the data and set up
file = fopen('sis1.dat');
custOrders = fscanf(file, '%d');
day = 1:100;
invLevel = [];
toOrder = [];
costAr = [];
costPer = [];
% for optimal s later
setupCost = 1000;

for s = 1:80
    for i = 1:100

        % find your working inventory
        workInv = workInv - custOrders(i);

        % if you fell below minimum
        if workInv < s
            cost = cost + 700;
            % get the required order amount 
            orderReq = S - workInv;

            % if you need to order S or more, max is S
            if orderReq > S
                orderReq = S;
            end

            % append values and re-calc working inventory so you
            % don't append a negative value (refills are instant from above)
            toOrder = [toOrder, orderReq];
            workInv = S - custOrders(i); 
            invLevel = [invLevel, workInv];

        else

            % append values, no extra stuff needed since you didn't
            % fall below the minimum
            toOrder = [toOrder, 0];
            invLevel = [invLevel, workInv];
            cost = cost + 25;
        end

    end
    
    costAr = [costAr, cost];
    cost = 0;
    totalCost = 0;
  
end

x = 1:80;
% draw a scatter plot
scatter(x, costAr);
ylim([25000 80000])
xlim([1 80])
xlabel('Minimum Inv')
ylabel('Cost (in $10k)')

[minVal, indexMin] = min(costAr);
fprintf('Min is %i at %i\n', minVal, indexMin);


% re-orient for table use
day = day';
invLevel = invLevel';
toOrder = toOrder';



% close file when done
fclose(file);
