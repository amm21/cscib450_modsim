% Alexis Miller
% CSCI B450 Mod & Sim: Project 4

clear;
clc;

i = [1 2 3 4 5 6 7 8 9 10]; % customer id 
ai = [15 47 81 111 123 152 176 226 310 330]; % arrival time for each customer
si = [43 36 34 30 38 40 31 29 36 30]; % service time for each customer
di = []; % delay time for each customer
ci = []; % completion time for each customer
wi = []; % waiting time for each customer
min_delay = 0; % no one can wait a negative amount of time, min is 0

% calculate everything
for x = 1:length(i)
    
    % the first person should have 0 delay because 
    % no one was in front of them in line
    if x == 1
        di = [di, 0];
    % everyone else has a delay of previous completion
    % time minus current arrival time 
    else
        new_d = ci(x-1) - ai(x);
        
        % no one can have a negative delay, if they do
        % then it means their delay was actually 0
        if new_d > min_delay
        
            di = [di, new_d];
        else 
            di = [di, min_delay];
        end
    end
    
    % completion time is arrival + service + delay,
    % append it 
    new_c = ai(x) + si(x) + di(x);
    ci = [ci, new_c];
    
    % wait time is service + delay, append it
    new_w = si(x) + di(x);
    wi = [wi, new_w];
    
end


% print everything in a readable way
fprintf('<strong>i</strong>\t\t')
for x = 1:length(i)
    fprintf('%i\t\t', i(x))
end
fprintf('\n')

fprintf('<strong>ai</strong>\t\t')
for x = 1:length(i)
    fprintf('%i\t\t', ai(x))
end
fprintf('\n')

 fprintf('<strong>si</strong>\t\t')
for x = 1:length(i)
    fprintf('%i\t\t', si(x))
end
fprintf('\n')

fprintf('<strong>di</strong>\t\t')
for x = 1:length(i)
    fprintf('%i\t\t', di(x))
end
fprintf('\n')

fprintf('<strong>ci</strong>\t\t')
for x = 1:length(i)
    fprintf('%i\t\t', ci(x))
end
fprintf('\n')

fprintf('<strong>wi</strong>\t\t')
for x = 1:length(i)
    fprintf('%i\t\t', wi(x))
end
fprintf('\n')