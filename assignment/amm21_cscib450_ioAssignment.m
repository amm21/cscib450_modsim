%{
Alexis Miller
CSCI B450 Mod & Sim
IO Assignment
%}

clear;
clc;

% get the data and set up
%file = readtable('Assignment.xls');
% get only your numbers from excel
num = xlsread('Assignment.xls');

% calculate and print average
average = mean(num(:, 2));
fprintf('Average: %f\n', average);

% calculate and print standard deviation
stdDev = std(num(:,2));
fprintf('Standard Deviation: %f\n', stdDev);


n = length(num); % total num values
% !! Confidence Intervals Below Here !! %
% Please note: I do not have the toolbox that would be required to
% use the tinv built-in function. I do know it's easier, but I did this 
% using the actual formulas because it's very expensive to purchase the 
% extra license. These are the formulas from the slides.
%conf90 = tinv(1-0.1/2, n-1); 
%conf95 = tinv(1-0.05/2, n-1);s
%conf99 = tinv(1-0.01/2, n-1);

% get the z values for each confidence interval from a z table
z90 = 1.645;
z95 = 1.960;
z99 = 2.576;

% 90% confidence
con90 = z90 * (stdDev / sqrt(n) );
low90 = average - con90;
high90 = average + con90;
fprintf('90 confidence is %f +- %f OR from %f to %f\n', average, con90, low90, high90);

% 95% confidence
con95 = z95 * (stdDev / sqrt(n) );
low95 = average - con95;
high95 = average + con95;
fprintf('95 confidence is %f +- %f OR from %f to %f\n', average, con95, low95, high95);

% 99% confidence
con99 = z99 * (stdDev / sqrt(n) );
low99 = average - con99;
high99 = average + con99;
fprintf('99 confidence is %f +- %f OR from %f to %f\n', average, con99, low99, high99);
