% algo15_2.m
clear
clc
%-----------------------------------------------------------------------
demandfile = 'ssq1.dat';  % input file
[as] = textread(demandfile); % arrival and service time
 
a = diff(as(:,1)); % get inter-arrival time between customers
mu_a= mean(a);
% s_a = std(d);
 
figure(1)
subplot(2,2,1)
[na, center_a] = hist(a);
bar(center_a, na);
title('histogram of inter-arrival time')
 
j = 1:length(a);
b = exprnd(mu_a, [1, length(a)]); 
% alternatively
% b = expinv((j-0.5)/length(a), mu_a); % obtain inverse of the exponential 
% cdf with parameters specified by mean parameter mu for the corresponding 
% probabilities of (j-0.5)/length(a)
% 
subplot(2,2,2)
qqplot(a,b) % or qqplot(a,c)
title('Q-Q plot for inter-arrival time, exponential distribution')
%-----------------------------------------------------------------------
s = as(:,2);  % service time 
mu_s = mean(s);
s_s = std(s);
subplot(2,2,3)
[ns, center_s] = hist(s);
bar(center_s, ns);
title('histogram of service time')
 
k = 1:length(s);
s_b = exprnd(mu_s, [1,length(a)]);
%alternatively, s_b = expinv((k-0.5)/length(s), mu_s); % obtain inverse of the exponential 
% cdf with parameters specified by mean parameter mu for the corresponding 
% probabilities of (j-0.5)/length(a) 
subplot(2,2,4)
qqplot(s,s_b)
title('Q-Q plot for service time, exponential distribution')
 
figure (2) % to check whether service time follows uniform distribution
smin = 1.0;
smax = 2.0;
s_bu = unifrnd(smin, smax, [1,length(s)]);
% alternatively
% s_bu = unifinv((k-0.5)/length(s), smin, smax );  

subplot(1,2,1)
qqplot(s,s_bu)
title('serivce time, uniform distribution?')
s_bp = poissrnd(mu_s, [1,length(s)]);
% alternatively s_bp = poissinv((k-0.5)/length(s), mu_s ); 
subplot(1,2,2)
qqplot(s,s_bp)
title('serivce time, poisson distribution?')

%------------------------------------------------------
expectedFreqofa = exppdf(center_a, mu_a)'*sum(na);
justificationCoefficent_a = sum(na)/sum(expectedFreqofa); 
% to scale to same total samples
expectedFreqofa  = expectedFreqofa *justificationCoefficent_a;
 
expectedFreqofs = exppdf(center_s, mu_s)'*sum(ns);
justificationCoefficent_s = sum(ns)/sum(expectedFreqofs); 
% to scale to same total samples
expectedFreqofs  = expectedFreqofs *justificationCoefficent_s;
 
[center_a' na' expectedFreqofa]
[center_s' ns' expectedFreqofs]
 
% xlswrite('inputAnalysisExample1.xlsx', [center_a' na' expectedFreqofa], 'Sheet1')
% xlswrite('inputAnalysisExample1.xlsx', [center_s' ns' expectedFreqofs], 'Sheet2')

% then work on the excel sheet.

